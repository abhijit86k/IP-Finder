/*
 * IP-Finder GNOME Extension by ArcMenu Team
 * https://gitlab.com/arcmenu-team/IP-Finder
 *
 * ArcMenu Team
 * Andrew Zaech https://gitlab.com/AndrewZaech
 * LinxGem33 (Andy C) https://gitlab.com/LinxGem33
 *
 * Find more from ArcMenu Team at
 * https://gitlab.com/arcmenu-team
 * https://github.com/ArcMenu
 *
 * Credits: _syncMainConnection(), _mainConnectionStateChanged()
 *  _flushConnectivityQueue(), _closeConnectivityCheck(), _portalHelperDone(), _syncConnectivity()
 * borrowed from GNOME shell.
 *
 * This file is part of IP Finder gnome extension.
 * IP Finder gnome extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * IP Finder gnome extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IP Finder gnome extension.  If not, see <http://www.gnu.org/licenses/>.
 */

const ExtensionUtils = imports.misc.extensionUtils
const Me = ExtensionUtils.getCurrentExtension();

const {Clutter, GLib, Gio, GObject, Meta, NM, Soup, Shell, St} = imports.gi;
const Clipboard = St.Clipboard.get_default();
const CLIPBOARD_TYPE = St.ClipboardType.CLIPBOARD;
const Gettext = imports.gettext.domain(Me.metadata['gettext-domain']);
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Utils = Me.imports.utils;
const Util = imports.misc.util;
const _ = Gettext.gettext;

const { loadInterfaceXML } = imports.misc.fileUtils;

const PortalHelperIface = loadInterfaceXML('org.gnome.Shell.PortalHelper');
const PortalHelperInfo = Gio.DBusInterfaceInfo.new_for_xml(PortalHelperIface);

const DEBUG_LOG = false;

const PortalHelperResult = {
    CANCELLED: 0,
    COMPLETED: 1,
    RECHECK: 2,
};

const ICON_SIZE = 16;

const DEFAULT_MAP_TILE = Me.path + '/icons/default_map.png';
const LATEST_MAP_TILE = Me.path + '/icons/latest_map.png';

const VPN_Widgets = {
    ALL: 0,
    ICON_ONLY: 1,
    TEXT_ONLY: 2,
}

const PanelActors = {
    FLAG_IP: 0,
    FLAG: 1,
    IP: 2,
}

const DefaultIpData = {
    ip: { name: _("IP Address"), text: _("Loading IP Details")},
    hostname: { name: _("Hostname"), text: ''},
    city: { name: _("City"), text: ''},
    region: { name: _("Region"), text: ''},
    country: { name: _("Country"), text: ''},
    loc: { name: _("Location"), text: ''},
    org: { name: _("Org"), text: ''},
    postal: { name: _("Postal"), text: ''},
    timezone: { name: _("Timezone"), text: ''},
};

function debugLog(msg){
    if(!DEBUG_LOG)
        return
    
    log(msg);
}

var VpnInfoBox = GObject.registerClass(
class IP_Finder_VpnInfoBox extends St.BoxLayout {
    _init(params) {
        super._init({
            ...params
        });

        this._vpnTitleLabel = new St.Label({
            style_class: 'ip-info-vpn-off',
            text: _("VPN") + ': ',
            x_align: Clutter.ActorAlign.FILL,
            y_align: Clutter.ActorAlign.START,
            y_expand: false,
        });
        this.add_child(this._vpnTitleLabel);
        this._vpnStatusLabel = new St.Label({
            x_align: Clutter.ActorAlign.FILL,
            y_align: Clutter.ActorAlign.START,
            x_expand: true,
            y_expand: false,
            style_class: 'ip-info-vpn-off',
        });
        this.add_child(this._vpnStatusLabel);

        this._vpnIcon = new St.Icon({
            style_class: 'popup-menu-icon ip-info-vpn-off'
        });
        this.add_child(this._vpnIcon);
    }

    setVpnStatus(vpnStatus){
        this._vpnTitleLabel.set_style_class_name(vpnStatus.styleClass);
        this._vpnStatusLabel.set_style_class_name(vpnStatus.styleClass);
        this._vpnIcon.set_style_class_name(`popup-menu-icon ${vpnStatus.styleClass}`);

        this._vpnStatusLabel.text = vpnStatus.vpnOn ? vpnStatus.vpnName : _('Off');
        this._vpnIcon.gicon = Gio.icon_new_for_string(vpnStatus.iconPath);
    }
});

var BaseButton = GObject.registerClass(
class IP_Finder_BaseButton extends St.Button {
    _init(text, params) {
        super._init({
            reactive: true,
            can_focus: true,
            track_hover: true,
            button_mask: St.ButtonMask.ONE | St.ButtonMask.TWO,
            ...params
        });

        this.connect('notify::hover', () => this._onHover());
        this.connect('destroy', () => this._onDestroy());

        this.tooltipLabel = new St.Label({
            style_class: 'dash-label tooltip-label',
            text: _(text)
        });
        this.tooltipLabel.hide();
        global.stage.add_child(this.tooltipLabel);
    }

    _onHover() {
        if(this.hover)
            this.showLabel();
        else
            this.hideLabel();
    }

    showLabel() {
        this.tooltipLabel.opacity = 0;
        this.tooltipLabel.show();

        let [stageX, stageY] = this.get_transformed_position();

        const itemWidth = this.allocation.get_width();
        const itemHeight = this.allocation.get_height();

        const labelWidth = this.tooltipLabel.get_width();
        const labelHeight = this.tooltipLabel.get_height();
        const offset = 6;
        const xOffset = Math.floor((itemWidth - labelWidth) / 2);

        let monitorIndex = Main.layoutManager.findIndexForActor(this);
        let workArea = Main.layoutManager.getWorkAreaForMonitor(monitorIndex);

        let x, y;
        x = Math.clamp(stageX + xOffset, 0 + offset, workArea.x + workArea.width - labelWidth - offset);

        //Check if should place tool-tip above or below app icon
        //Needed in case user has moved the panel to bottom of screen
        let labelBelowIconRect = new Meta.Rectangle({
            x,
            y: stageY + itemHeight + offset,
            width: labelWidth,
            height: labelHeight
        });

        if(workArea.contains_rect(labelBelowIconRect))
            y = labelBelowIconRect.y;
        else
            y = stageY - labelHeight - offset;

        this.tooltipLabel.remove_all_transitions();
        this.tooltipLabel.set_position(x, y);
        this.tooltipLabel.ease({
            opacity: 255,
            duration: 250,
            mode: Clutter.AnimationMode.EASE_OUT_QUAD,
        });
    }

    hideLabel() {
        this.tooltipLabel.ease({
            opacity: 0,
            duration: 100,
            mode: Clutter.AnimationMode.EASE_OUT_QUAD,
            onComplete: () => this.tooltipLabel.hide(),
        });
    }

    _onDestroy(){
        this.tooltipLabel.remove_all_transitions();
        this.tooltipLabel.hide();
        global.stage.remove_child(this.tooltipLabel);
        this.tooltipLabel.destroy();
    }
});

var IPFinderMenuButton = GObject.registerClass(class IP_Finder_MenuButton extends PanelMenu.Button{
    _init() {
        super._init(0.5, _('IP Details'));
        this.menu.box.style = "padding: 16px;";

        this._settings = ExtensionUtils.getSettings();
        this._extensionConnections = new Map();
        this._createSettingsConnections();

        this._textureCache = St.TextureCache.get_default();
        this._session = new Soup.Session({ user_agent: `ip-finder/'${Me.metadata.version}`, timeout: 15 });

        let panelBox = new St.BoxLayout({
            x_align: Clutter.ActorAlign.FILL,
            y_align: Clutter.ActorAlign.FILL,
        });
        this.add_actor(panelBox);

        this._vpnStatusIcon = new St.Icon({
            icon_name: 'changes-prevent-symbolic',
            icon_size: ICON_SIZE,
            x_align: Clutter.ActorAlign.START,
            y_align: Clutter.ActorAlign.CENTER,
            style: "padding-right: 5px;"
        });
        panelBox.add_child(this._vpnStatusIcon);

        this._ipAddress = DefaultIpData.ip.text;
        this._ipAddressLabel = new St.Label({
            text: this._ipAddress,
            y_align: Clutter.ActorAlign.CENTER
        });
        panelBox.add_actor(this._ipAddressLabel);

        this._panelIcon = new St.Icon({
            icon_name: 'network-wired-acquiring-symbolic',
            icon_size: ICON_SIZE,
            x_align: Clutter.ActorAlign.START,
            y_align: Clutter.ActorAlign.CENTER,
            style: "padding-left: 5px; padding-top: 3px;"
        });
        panelBox.add_actor(this._panelIcon);

        let menuSection = new PopupMenu.PopupMenuSection();
        this.menu.addMenuItem(menuSection);

        let mapAndIpDetailsBox = new St.BoxLayout({
            x_align: Clutter.ActorAlign.FILL,
            x_expand: true,
            style: "min-width:540px; padding-bottom: 10px;"
        });
        menuSection.actor.add(mapAndIpDetailsBox);

        this._mapTileBox = new St.BoxLayout({
            vertical: true,
            x_align: Clutter.ActorAlign.CENTER,
            y_align: Clutter.ActorAlign.CENTER,
            y_expand: true,
        });
        mapAndIpDetailsBox.add_actor(this._mapTileBox);
        this._mapTileBox.add_actor(this._getMapTileIcon(DEFAULT_MAP_TILE));

        let ipInfoParentBox = new St.BoxLayout({
            style_class: 'ip-info-box',
            vertical: true,
            x_align: Clutter.ActorAlign.CENTER,
        });
        mapAndIpDetailsBox.add_actor(ipInfoParentBox);

        this._vpnInfoBox = new VpnInfoBox();
        ipInfoParentBox.add_actor(this._vpnInfoBox);

        this._ipInfoBox = new St.BoxLayout({
            vertical: true,
        });
        ipInfoParentBox.add_actor(this._ipInfoBox);

        let buttonBox = new St.BoxLayout();
        menuSection.actor.add_actor(buttonBox);

        let settingsButton = new BaseButton(_('Settings'), {
            icon_name: 'emblem-system-symbolic',
            style_class: 'button baseButton'
        });
        settingsButton.connect('clicked',  ()=> {
            ExtensionUtils.openPrefs();
            this.menu.toggle();
        });
        buttonBox.add_actor(settingsButton);

        let copyButton = new BaseButton(_('Copy IP'), {
            icon_name: 'edit-copy-symbolic',
            x_expand: true,
            x_align: Clutter.ActorAlign.CENTER,
            style_class: 'button baseButton'
        });
        copyButton.connect('clicked',  ()=> Clipboard.set_text(CLIPBOARD_TYPE, this._ipAddress) );
        buttonBox.add_actor(copyButton);

        let refreshButton = new BaseButton(_('Refresh'), {
            icon_name: 'view-refresh-symbolic',
            x_expand: false,
            x_align: Clutter.ActorAlign.END,
            style_class: 'button baseButton'
        });
        refreshButton.connect('clicked', () => this._getIpInfo().catch(err => log(err)) );
        buttonBox.add_actor(refreshButton);

        NM.Client.new_async(null, this.establishNetworkConnectivity.bind(this));

        Main.panel.addToStatusArea('ip-menu', this, 1, this._settings.get_string('position-in-panel'));
        this._updatePanelWidgets();
        this._updateVPNWidgets();
    }

    _createSettingsConnections(){
        this._extensionConnections.set(this._settings.connect('changed::vpn-status', () => this._updateVPNWidgets()), this._settings);
        this._extensionConnections.set(this._settings.connect('changed::vpn-widgets', () => this._updateVPNWidgets()), this._settings);
        this._extensionConnections.set(this._settings.connect('changed::vpn-status-only-when-on', () => this._updateVPNWidgets()), this._settings);
        this._extensionConnections.set(this._settings.connect('changed::vpn-icon-color', () => this._updateVPNWidgets()), this._settings);
        this._extensionConnections.set(this._settings.connect('changed::vpn-ip-address-color', () => this._updateVPNWidgets()), this._settings);
        this._extensionConnections.set(this._settings.connect('changed::position-in-panel', () => this._updatePosition()), this._settings);
        this._extensionConnections.set(this._settings.connect('changed::actors-in-panel', () => this._updatePanelWidgets()), this._settings);
        this._extensionConnections.set(this._settings.connect('changed::vpn-connections-whitelist', () => this._getIpInfo().catch(err => log(err))), this._settings);
    }

    _clearConnections(){
        this._extensionConnections.forEach((object, id) => {
            if(id)
                object.disconnect(id);
        });
        this._extensionConnections = null;
    }

    _updatePosition() {
        Main.panel.statusArea['ip-menu'] = null;
        Main.panel.addToStatusArea('ip-menu', this, 1, this._settings.get_string('position-in-panel'));
    }

    _updatePanelWidgets(){
        const panelActors = this._settings.get_enum('actors-in-panel');
        if(panelActors === PanelActors.FLAG_IP){
            this._panelIcon.show();
            this._ipAddressLabel.show();
        }
        else if(panelActors === PanelActors.FLAG){
            this._panelIcon.show();
            this._ipAddressLabel.hide();
        }
        else if(panelActors === PanelActors.IP){
            this._panelIcon.hide();
            this._ipAddressLabel.show();
        }
    }

    _updateVPNWidgets(){
        const showWhenActiveVpn = this._settings.get_boolean('vpn-status-only-when-on') ? this._vpnConnectionOn : true;
        const showVpnStatus = this._settings.get_boolean('vpn-status') && showWhenActiveVpn;

        this._vpnStatusIcon.visible = showVpnStatus && this._settings.get_enum('vpn-widgets') !== VPN_Widgets.TEXT_ONLY;
        this._vpnInfoBox.visible = showVpnStatus && this._settings.get_enum('vpn-widgets') !== VPN_Widgets.ICON_ONLY;

        this._vpnStatusIcon.icon_name = this._vpnConnectionOn ? 'changes-prevent-symbolic' : 'changes-allow-symbolic';
        if(this._settings.get_boolean('vpn-icon-color'))
            this._vpnStatusIcon.style_class = this._vpnConnectionOn ? 'ip-info-vpn-on' : 'ip-info-vpn-off';
        else
            this._vpnStatusIcon.style_class = null;

        if(this._settings.get_boolean('vpn-ip-address-color'))
            this._ipAddressLabel.style_class = this._vpnConnectionOn ? 'ip-info-vpn-on' : 'ip-info-vpn-off';
        else
            this._ipAddressLabel.style_class = null;
    }

    establishNetworkConnectivity(obj, result){
        this._client = NM.Client.new_finish(result);

        this._connectivityQueue = new Set();

        this._mainConnection = null;

        this._client.connectObject(
            'notify::primary-connection', () => this._syncMainConnection(),
            'notify::activating-connection', () => this._syncMainConnection(),
            'notify::active-connections', () => this._syncMainConnection(),
            'notify::connectivity', () => this._syncConnectivity(),
            this);
        this._syncMainConnection();
    }

    _syncMainConnection() {
        this._setAcquiringDetials();
        this._mainConnection?.disconnectObject(this);

        this._mainConnection =
            this._client.get_primary_connection() ||
            this._client.get_activating_connection();

        if (this._mainConnection) {
            this._mainConnection.connectObject('notify::state',
                this._mainConnectionStateChanged.bind(this), this);
            this._mainConnectionStateChanged();
        }

        this._syncConnectivity();
    }

    _mainConnectionStateChanged() {
        if (this._mainConnection.state === NM.ActiveConnectionState.ACTIVATED){
            this._removeGetIpInfoId();

            this._setAcquiringDetials();

            this._getIpInfoId = GLib.timeout_add(0, 2000, () => {
                this._getIpInfo().catch(err => log(err));
                this._getIpInfoId = null;
                return GLib.SOURCE_REMOVE;
            });
        }
    }

    _removeGetIpInfoId(){
        if(this._getIpInfoId){
            GLib.source_remove(this._getIpInfoId);
            this._getIpInfoId = null;
        }
    }

    _flushConnectivityQueue() {
        for (let item of this._connectivityQueue)
            this._portalHelperProxy?.CloseAsync(item);
        this._connectivityQueue.clear();
    }

    _closeConnectivityCheck(path) {
        if (this._connectivityQueue.delete(path))
            this._portalHelperProxy?.CloseAsync(path);
    }

    async _portalHelperDone(proxy, emitter, parameters) {
        let [path, result] = parameters;

        if (result == PortalHelperResult.CANCELLED) {
            this._loadDetails(null, null);
            // Keep the connection in the queue, so the user is not
            // spammed with more logins until we next flush the queue,
            // which will happen once they choose a better connection
            // or we get to full connectivity through other means
        } else if (result == PortalHelperResult.COMPLETED) {
            this._getIpInfo().catch(err => log(err));
            this._closeConnectivityCheck(path);
        } else if (result == PortalHelperResult.RECHECK) {
            this._loadDetails(null, null);
            try {
                const state = await this._client.check_connectivity_async(null);
                if (state >= NM.ConnectivityState.FULL){
                    this._getIpInfo().catch(err => log(err));
                    this._closeConnectivityCheck(path);
                }
                    
            } catch (e) { }
        } else {
            this._loadDetails(null, `Invalid result from portal helper: ${result}`);
        }
    }

    async _syncConnectivity() {
        if(this._client.get_active_connections().length < 1 || this._client.connectivity === NM.ConnectivityState.NONE)
            this._loadDetails(null, null);

        if (this._mainConnection == null ||
            this._mainConnection.state != NM.ActiveConnectionState.ACTIVATED) {
            this._loadDetails(null, null);
            this._flushConnectivityQueue();
            return;
        }

        let isPortal = this._client.connectivity == NM.ConnectivityState.PORTAL;
        // For testing, allow interpreting any value != FULL as PORTAL, because
        // LIMITED (no upstream route after the default gateway) is easy to obtain
        // with a tethered phone
        // NONE is also possible, with a connection configured to force no default route
        // (but in general we should only prompt a portal if we know there is a portal)
        if (GLib.getenv('GNOME_SHELL_CONNECTIVITY_TEST') != null)
            isPortal ||= this._client.connectivity < NM.ConnectivityState.FULL;
        if (!isPortal)
            return;

        let path = this._mainConnection.get_path();
        if (this._connectivityQueue.has(path))
            return;

        let timestamp = global.get_current_time();
        if (!this._portalHelperProxy) {
            this._portalHelperProxy = new Gio.DBusProxy({
                g_connection: Gio.DBus.session,
                g_name: 'org.gnome.Shell.PortalHelper',
                g_object_path: '/org/gnome/Shell/PortalHelper',
                g_interface_name: PortalHelperInfo.name,
                g_interface_info: PortalHelperInfo,
            });
            this._portalHelperProxy.connectSignal('Done',
                () => this._portalHelperDone().catch(logError));

            try {
                await this._portalHelperProxy.init_async(
                    GLib.PRIORITY_DEFAULT, null);
            } catch (e) {
                console.error(`Error launching the portal helper: ${e.message}`);
            }
        }

        this._portalHelperProxy?.AuthenticateAsync(path, this._client.connectivity_check_uri, timestamp).catch(logError);

        this._connectivityQueue.add(path);
    }

    async _getIpInfo(){
        this._setAcquiringDetials();

        this._vpnConnectionOn = false;
        this._vpnConnectionName = null;

        if(this._client.connectivity === NM.ConnectivityState.NONE){
            this._loadDetails(null, null);
            return;
        }

        const whiteList = this._settings.get_strv('vpn-connections-whitelist');
        let activeConnectionIds = [];
        let activeConnections = this._client.get_active_connections() || [];

        const handledTypes = [
            'vpn',
            'wireguard',
            'tun',
            'ppp'
        ]

        debugLog('IP-Finder Log');
        debugLog('Active Connections--------------------------');
        activeConnections.forEach(a => {
            activeConnectionIds.push(a.id);
            if(handledTypes.includes(a.type) || whiteList.includes(a.id)){
                debugLog(`VPN Connection: '${a.id}', Type: '${a.type}'`);
                this._vpnConnectionOn = true;
                this._vpnConnectionName = a.id;
            }
            else
                debugLog(`Connection: '${a.id}', Type: '${a.type}'`);
        });
        debugLog('--------------------------------------------');
        debugLog('');

        this._settings.set_strv('current-connection-ids', activeConnectionIds);

        this._session = new Soup.Session({ user_agent: `ip-finder/'${Me.metadata.version}`, timeout: 15 });

        if(activeConnections.length < 1){
            this._loadDetails(null, null);
            return;
        }

        let [ipAddress, ipError] = await Utils.getIP(this._session);

        if(ipAddress){
            let [ipDetails, ipDetailsError] = await Utils.getIPDetails(this._session, ipAddress);
            this._loadDetails(ipDetails, ipDetailsError);
        }
        else
            this._loadDetails(null, ipError);
    }

    _setAcquiringDetials(){
        this._panelIcon.show();
        this._ipAddressLabel.text = _(DefaultIpData.ip.text);
        this._ipAddressLabel.style_class = null;
        this._panelIcon.icon_name = 'network-wired-acquiring-symbolic';
        this._vpnStatusIcon.style_class = null;
        this._vpnStatusIcon.hide();
        this._vpnInfoBox.hide();
    }

    _loadDetails(data, error){
        this._ipInfoBox.destroy_all_children();
        this._mapTileBox.destroy_all_children();

        //null data indicates no connection found or error in gathering ip info
        if(!data){
            this._ipAddressLabel.style_class = null;
            this._ipAddressLabel.text = error ? _('Error!') : _("No Connection");
            this._panelIcon.icon_name = 'network-offline-symbolic';
            this._vpnStatusIcon.style_class = null;

            let ipInfoRow = new St.BoxLayout();
            this._ipInfoBox.add_actor(ipInfoRow);

            let label = new St.Label({
                style_class: 'ip-info-key',
                text: error ? `${error}` : _("No Connection"),
                x_align: Clutter.ActorAlign.CENTER,
                x_expand: true
            });
            ipInfoRow.add_actor(label);

            this._mapTileBox.add_actor(this._getMapTileIcon(DEFAULT_MAP_TILE));
            return;
        }

        this._ipAddress = data.ip;
        this._ipAddressLabel.text = this._ipAddress;
        this._panelIcon.icon_name = '';
        this._panelIcon.gicon = Gio.icon_new_for_string(Me.path + '/icons/flags/' + data.country + '.png');

        this._vpnInfoBox.setVpnStatus({
            vpnOn: this._vpnConnectionOn,
            iconPath: this._vpnConnectionOn ? 'changes-prevent-symbolic' : 'changes-allow-symbolic',
            vpnName: this._vpnConnectionName ? this._vpnConnectionName : _("On"),
            styleClass: this._vpnConnectionOn ? 'ip-info-vpn-on' : 'ip-info-vpn-off',
        });

        this._updatePanelWidgets();
        this._updateVPNWidgets();

        this._ipInfoBox.add_actor(new PopupMenu.PopupSeparatorMenuItem());

        for(let key in DefaultIpData){
            if(data[key]){
                let ipInfoRow = new St.BoxLayout();
                this._ipInfoBox.add_actor(ipInfoRow);

                let label = new St.Label({
                    style_class: 'ip-info-key',
                    text: _(DefaultIpData[key].name) + ': ',
                    x_align: Clutter.ActorAlign.FILL,
                    y_align: Clutter.ActorAlign.CENTER,
                    y_expand: true,
                });
                ipInfoRow.add_actor(label);

                let infoLabel = new St.Label({
                    x_align: Clutter.ActorAlign.FILL,
                    y_align: Clutter.ActorAlign.CENTER,
                    x_expand: true,
                    y_expand: true,
                    style_class: 'ip-info-value',
                    text: data[key]
                });
                let dataLabelBtn = new St.Button({
                    child: infoLabel,
                });
                dataLabelBtn.connect('button-press-event', () => {
                    Clipboard.set_text(CLIPBOARD_TYPE, dataLabelBtn.child.text);
                });
                ipInfoRow.add_actor(dataLabelBtn);
            }
        }

        this._ipInfoBox.add_actor(new PopupMenu.PopupSeparatorMenuItem());

        const location = data['loc'];
        this._setMapTile(location).catch(e => log(e));
    }

    async _setMapTile(location){
        const mapTileInfo = Utils.getMapTileInfo(location);
        const mapTileCoordinates = `${mapTileInfo.xTile},${mapTileInfo.yTile}`;
        const mapTileUrl = `${mapTileInfo.zoom}/${mapTileInfo.xTile}/${mapTileInfo.yTile}`;

        if(mapTileCoordinates !== this._settings.get_string('map-tile-coords') || !this._checkLatestFileMapExists()){
            this._mapTileBox.add_actor(this._getMapTileIcon(DEFAULT_MAP_TILE));
            const mapLabel = new St.Label({
                style_class: 'ip-info-key',
                text: _("Loading new map tile..."),
                x_align: Clutter.ActorAlign.CENTER,
            });
            this._mapTileBox.add_actor(mapLabel);

            let [file, error] = await Utils.getMapTile(this._session, mapTileUrl);

            if(error)
                mapLabel.text = _("Error getting map tile: " + error);
            else{
                this._settings.set_string('map-tile-coords', mapTileCoordinates);
                this._mapTileBox.destroy_all_children();
                this._mapTileBox.add_actor(this._textureCache.load_file_async(file, -1, 200, 1, 1));
            }
            return;
        }

        this._mapTileBox.add_actor(this._getMapTileIcon(LATEST_MAP_TILE));
    }

    _getMapTileIcon(mapTile){
        if(mapTile === DEFAULT_MAP_TILE)
            return new St.Icon({ gicon: Gio.icon_new_for_string(mapTile), icon_size: 200 });
        else if (mapTile === LATEST_MAP_TILE)
            return this._textureCache.load_file_async(Gio.file_new_for_path(LATEST_MAP_TILE), -1, 200, 1, 1);
    }

    _checkLatestFileMapExists(){
        let file = Gio.File.new_for_path(LATEST_MAP_TILE);
        return file.query_exists(null);
    }

    disable() {
        this._removeGetIpInfoId();

        this._client?.disconnectObject(this);

        this._clearConnections();

        this._settings.run_dispose();
        this._settings = null;
    }
});

function init() {
    ExtensionUtils.initTranslations();
}

let _indicator;

function enable() {
    _indicator = new IPFinderMenuButton();
}

function disable() {
    _indicator.disable();
    _indicator.destroy();
    _indicator = null;
}
