/*
 * IP-Finder GNOME Extension by ArcMenu Team
 * https://gitlab.com/arcmenu-team/IP-Finder
 *
 * ArcMenu Team
 * Andrew Zaech https://gitlab.com/AndrewZaech
 * LinxGem33 (Andy C) https://gitlab.com/LinxGem33
 *
 * Find more from ArcMenu Team at
 * https://gitlab.com/arcmenu-team
 * https://github.com/ArcMenu
 *
 *
 * This file is part of IP Finder gnome extension.
 * IP Finder gnome extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * IP Finder gnome extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IP Finder gnome extension.  If not, see <http://www.gnu.org/licenses/>.
 */

const {Adw, Gdk, GdkPixbuf, Gio, GLib, GObject, Gtk} = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Gettext = imports.gettext.domain(Me.metadata['gettext-domain']);
const _ = Gettext.gettext;

const PROJECT_TITLE = _('IP Finder');
const PROJECT_DESCRIPTION = _('Displays useful information about your public IP Address');
const PROJECT_IMAGE = 'default_map';

var GeneralPage = GObject.registerClass(
class IPFinder_GeneralPage extends Adw.PreferencesPage {
    _init(settings) {
        super._init({
            title: _("General"),
            icon_name: 'preferences-system-symbolic',
            name: 'GeneralPage'
        });

        this._settings = settings;

        let generalGroup = new Adw.PreferencesGroup({
            title: _('General')
        });
        this.add(generalGroup);

        let actorsInPanelList = new Gtk.StringList();
        actorsInPanelList.append(_("IP Address and Flag"));
        actorsInPanelList.append(_("Flag"));
        actorsInPanelList.append(_("IP Address"));
        let actorsInPanelMenu = new Gtk.DropDown({
            valign: Gtk.Align.CENTER,
            model: actorsInPanelList,
            selected: this._settings.get_enum('actors-in-panel')
        })
        let actorsInPanelRow = new Adw.ActionRow({
            title: _("Elements to show on the Panel"),
            activatable_widget: actorsInPanelMenu
        });
        actorsInPanelRow.add_suffix(actorsInPanelMenu);
        actorsInPanelMenu.connect("notify::selected", (widget) => {
            this._settings.set_enum('actors-in-panel', widget.selected);
        });
        generalGroup.add(actorsInPanelRow);

        let panelPositions = new Gtk.StringList();
        panelPositions.append(_("Left"));
        panelPositions.append(_("Center"));
        panelPositions.append(_("Right"));
        let panelPositionRow = new Adw.ComboRow({
            title: _("Position in Panel"),
            model: panelPositions,
            selected: this._settings.get_enum('position-in-panel')
        });
        panelPositionRow.connect("notify::selected", (widget) => {
            this._settings.set_enum('position-in-panel', widget.selected);
        });
        generalGroup.add(panelPositionRow);

        let vpnGroup = new Adw.PreferencesGroup({
            title: _('VPN Status'),
        });
        this.add(vpnGroup);
    
        let showVPNStatusRow = new Adw.ExpanderRow({
            title: _('Show VPN Status'),
            subtitle: _('Attempts to display VPN status. Works best when connecting VPN through GNOME'),
            show_enable_switch: true,
            enable_expansion: this._settings.get_boolean('vpn-status'),
        });
        vpnGroup.add(showVPNStatusRow);

        showVPNStatusRow.connect("notify::enable-expansion", (widget) => {
            this._settings.set_boolean('vpn-status',  widget.enable_expansion);
        });

        let vpnWidgetsList = new Gtk.StringList();
        vpnWidgetsList.append(_("Icon on Panel + Text in Menu"));
        vpnWidgetsList.append(_("Icon on Panel"));
        vpnWidgetsList.append(_("Text in Menu"));
        let vpnWidgetsMenu = new Gtk.DropDown({
            valign: Gtk.Align.CENTER,
            model: vpnWidgetsList,
            selected: this._settings.get_enum('vpn-widgets'),
        })
        let vpnWidgetsRow = new Adw.ActionRow({
            title: _("VPN status display options"),
            activatable_widget: vpnWidgetsMenu
        });
        vpnWidgetsRow.add_suffix(vpnWidgetsMenu);
        vpnWidgetsMenu.connect("notify::selected", (widget) => {
            this._settings.set_enum('vpn-widgets', widget.selected);
        });
        showVPNStatusRow.add_row(vpnWidgetsRow);

        let showVPNOnlyWhenOnSwitch = new Gtk.Switch({
            active: this._settings.get_boolean('vpn-status-only-when-on'),
            valign: Gtk.Align.CENTER,
        });
        showVPNOnlyWhenOnSwitch.connect('notify::active', (widget) => {
            this._settings.set_boolean('vpn-status-only-when-on', widget.get_active());
        });
        let showVPNOnlyWhenOnRow = new Adw.ActionRow({
            title: _('Only show VPN status when VPN detected'),
            activatable_widget: showVPNOnlyWhenOnSwitch
        });
        showVPNOnlyWhenOnRow.add_suffix(showVPNOnlyWhenOnSwitch);
        showVPNStatusRow.add_row(showVPNOnlyWhenOnRow);

        let vpnIconColorSwitch = new Gtk.Switch({
            active: this._settings.get_boolean('vpn-icon-color'),
            valign: Gtk.Align.CENTER,
        });
        vpnIconColorSwitch.connect('notify::active', (widget) => {
            this._settings.set_boolean('vpn-icon-color', widget.get_active());
        });
        let vpnIconColorRow = new Adw.ActionRow({
            title: _('Colorize VPN Icon based on VPN status'),
            activatable_widget: vpnIconColorSwitch
        });
        vpnIconColorRow.add_suffix(vpnIconColorSwitch);
        showVPNStatusRow.add_row(vpnIconColorRow);

        let vpnAddressColorSwitch = new Gtk.Switch({
            active: this._settings.get_boolean('vpn-ip-address-color'),
            valign: Gtk.Align.CENTER,
        });
        vpnAddressColorSwitch.connect('notify::active', (widget) => {
            this._settings.set_boolean('vpn-ip-address-color', widget.get_active());
        });
        let vpnAddressColorRow = new Adw.ActionRow({
            title: _('Colorize IP Address based on VPN status'),
            activatable_widget: vpnAddressColorSwitch
        });
        vpnAddressColorRow.add_suffix(vpnAddressColorSwitch);
        showVPNStatusRow.add_row(vpnAddressColorRow);

        let whiteListGroup = new Adw.PreferencesGroup({
            title: _('Whitelisted Connections'),
            description: _('Add a connection if its not automatically recognised by IP Finder')
        });
        this.add(whiteListGroup);

        this.currentConnectionsMenu = new Gtk.DropDown({
            valign: Gtk.Align.CENTER,
            halign: Gtk.Align.FILL,
        });

        this._settings.connect('changed::current-connection-ids', () => {
            this._populateCurrentConnectionsMenu();
        });
        this._populateCurrentConnectionsMenu();

        let addtoWhiteListButton = new Gtk.Button({
            label: _('Add'),
            valign: Gtk.Align.CENTER
        });
        addtoWhiteListButton.connect('clicked', () => {
            this.whiteListExpanderRow.expanded = true;
            const selectedConnection = this.currentConnectionsMenu.get_selected_item();
            const connectionId = selectedConnection.string;

            this._addConnectionToWhitelist(connectionId);

            let whitelist = this._settings.get_strv('vpn-connections-whitelist');
            whitelist.push(connectionId);

            this._settings.set_strv('vpn-connections-whitelist', whitelist);
        });
        let addToWhiteListRow = new Adw.ActionRow({
            title: _('Choose a connection to add to VPN Whitelist'),
            activatable_widget: addtoWhiteListButton
        });
        addToWhiteListRow.add_suffix(this.currentConnectionsMenu);
        addToWhiteListRow.add_suffix(addtoWhiteListButton);

        this.whiteListExpanderRow = new Adw.ExpanderRow({
            title: _('Whitelisted VPN Connections')
        });

        const whiteList = this._settings.get_strv('vpn-connections-whitelist');

        for(let i = 0; i < whiteList.length; i++){
            this._addConnectionToWhitelist(whiteList[i]);
        }

        whiteListGroup.add(addToWhiteListRow);
        whiteListGroup.add(this.whiteListExpanderRow);
    }

    _populateCurrentConnectionsMenu(){
        let currentConnectionsList = new Gtk.StringList();
        let currentConnectionIds = this._settings.get_strv('current-connection-ids');

        for(let i = 0; i < currentConnectionIds.length; i++)
            currentConnectionsList.append(currentConnectionIds[i]);

        this.currentConnectionsMenu.model = currentConnectionsList;
    }

    _addConnectionToWhitelist(title){
        let deleteEntry = new Gtk.Button({
            label: _('Delete'),
            valign: Gtk.Align.CENTER
        });
        deleteEntry.connect('clicked', () => {
            this.whiteListExpanderRow.remove(connectionRow);

            let whitelist = this._settings.get_strv('vpn-connections-whitelist');
            let index = whitelist.indexOf(title);
            whitelist.splice(index, 1);

            this._settings.set_strv('vpn-connections-whitelist', whitelist);
        });
        let connectionRow = new Adw.ActionRow({
            title: title,
            activatable_widget: deleteEntry
        });
        connectionRow.add_suffix(deleteEntry);
        this.whiteListExpanderRow.add_row(connectionRow);
    }
});

var AboutPage = GObject.registerClass(
class extends Adw.PreferencesPage {
    _init(settings) {
        super._init({
            title: _('About'),
            icon_name: 'help-about-symbolic',
            name: 'AboutPage'
        });
        this._settings = settings;

        //Project Logo, title, description-------------------------------------
        let projectHeaderGroup = new Adw.PreferencesGroup();
        let projectHeaderBox = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            hexpand: false,
            vexpand: false
        });

        let projectImage = new Gtk.Image({
            margin_bottom: 5,
            icon_name: PROJECT_IMAGE,
            pixel_size: 100,
        });

        let projectTitleLabel = new Gtk.Label({
            label: _(PROJECT_TITLE),
            css_classes: ['title-1'],
            vexpand: true,
            valign: Gtk.Align.FILL
        });

        let projectDescriptionLabel = new Gtk.Label({
            label: _(PROJECT_DESCRIPTION),
            hexpand: false,
            vexpand: false,
        });
        projectHeaderBox.append(projectImage);
        projectHeaderBox.append(projectTitleLabel);
        projectHeaderBox.append(projectDescriptionLabel);
        projectHeaderGroup.add(projectHeaderBox);

        this.add(projectHeaderGroup);
        //-----------------------------------------------------------------------

        //Extension/OS Info Group------------------------------------------------
        let infoGroup = new Adw.PreferencesGroup();

        let projectVersionRow = new Adw.ActionRow({
            title: `${PROJECT_TITLE} ${_('Version')}`,
        });
        projectVersionRow.add_suffix(new Gtk.Label({
            label: Me.metadata.version.toString()
        }));
        infoGroup.add(projectVersionRow);

        if(Me.metadata.commit){
            let commitRow = new Adw.ActionRow({
                title: _('Git Commit')
            });
            commitRow.add_suffix(new Gtk.Label({
                label: Me.metadata.commit.toString(),
            }));
            infoGroup.add(commitRow);
        }

        let gnomeVersionRow = new Adw.ActionRow({
            title: _('GNOME Version'),
        });
        gnomeVersionRow.add_suffix(new Gtk.Label({
            label: imports.misc.config.PACKAGE_VERSION.toString(),
        }));
        infoGroup.add(gnomeVersionRow);

        let osRow = new Adw.ActionRow({
            title: _('OS'),
        });

        let name = GLib.get_os_info("NAME");
        let prettyName = GLib.get_os_info("PRETTY_NAME");
        let buildID = GLib.get_os_info("BUILD_ID");
        let versionID = GLib.get_os_info("VERSION_ID");

        let osInfoText = prettyName ? prettyName : name;
        if(versionID)
            osInfoText += `; Version ID: ${versionID}`;
        if(buildID)
            osInfoText += `; Build ID: ${buildID}`;

        osRow.add_suffix(new Gtk.Label({
            label: osInfoText,
            single_line_mode: false,
            wrap: true,
        }));
        infoGroup.add(osRow);

        let sessionTypeRow = new Adw.ActionRow({
            title: _('Session Type'),
        });
        sessionTypeRow.add_suffix(new Gtk.Label({
            label: GLib.getenv('XDG_SESSION_TYPE') === "wayland" ? 'Wayland' : 'X11',
        }));
        infoGroup.add(sessionTypeRow);
        this.add(infoGroup);
        //-----------------------------------------------------------------------

        //Credits----------------------------------------------------------------
        let creditsGroup = new Adw.PreferencesGroup({
            title: _("Credits")
        });
        this.add(creditsGroup);

        let creditsRow = new Adw.PreferencesRow({
            activatable: false,
            selectable: false
        });
        creditsGroup.add(creditsRow);

        creditsRow.set_child(new Gtk.Label({
            label: CREDITS,
            use_markup: true,
            vexpand: true,
            valign: Gtk.Align.CENTER,
            margin_top: 5,
            margin_bottom: 20,
            hexpand: true,
            halign: Gtk.Align.FILL,
            justify: Gtk.Justification.CENTER
        }));
        //-----------------------------------------------------------------------

        let linksGroup = new Adw.PreferencesGroup();
        let linksBox = new Adw.ActionRow();

        let pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(Me.path + '/icons/donate-icon.svg', -1, 50, true);
        let donateImage = Gtk.Picture.new_for_pixbuf(pixbuf);
        let donateLinkButton = new Gtk.LinkButton({
            child: donateImage,
            uri: 'https://gnu.org/licenses/old-licenses/gpl-2.0.html',
        });

        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(Me.path + '/icons/gitlab-icon.svg', -1, 50, true);
        let gitlabImage = Gtk.Picture.new_for_pixbuf(pixbuf);
        let projectUrl = Me.metadata.url;
        let projectLinkButton = new Gtk.LinkButton({
            child: gitlabImage,
            uri: projectUrl,
        });

        linksBox.add_prefix(projectLinkButton);
        linksBox.add_suffix(donateLinkButton);
        linksGroup.add(linksBox);
        this.add(linksGroup);

        let gnuSoftwareGroup = new Adw.PreferencesGroup();
        let gnuSofwareLabel = new Gtk.Label({
            label: _(GNU_SOFTWARE),
            use_markup: true,
            justify: Gtk.Justification.CENTER
        });
        let gnuSofwareLabelBox = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            valign: Gtk.Align.END,
            vexpand: true,
        });
        gnuSofwareLabelBox.append(gnuSofwareLabel);
        gnuSoftwareGroup.add(gnuSofwareLabelBox);
        this.add(gnuSoftwareGroup);
    }
});

function init() {
    ExtensionUtils.initTranslations();
}

function fillPreferencesWindow(window) {
    let iconTheme = Gtk.IconTheme.get_for_display(Gdk.Display.get_default());
    if(!iconTheme.get_search_path().includes(Me.path + "/icons"))
        iconTheme.add_search_path(Me.path + "/icons");

    const settings = ExtensionUtils.getSettings();

    window.set_search_enabled(true);

    const generalPage = new GeneralPage(settings);
    window.add(generalPage);

    const aboutPage = new AboutPage();
    window.add(aboutPage);
}

var CREDITS = '\n <a href="https://gitlab.com/LinxGem33">LinxGem33</a> (Founder/Maintainer/Graphic Designer)'+
              '\n <a href="https://gitlab.com/AndrewZaech">AndrewZaech</a> (Developer)';

var GNU_SOFTWARE = '<span size="small">' +
    'This program comes with absolutely no warranty.\n' +
    'See the <a href="https://gnu.org/licenses/old-licenses/gpl-2.0.html">' +
    'GNU General Public License, version 2 or later</a> for details.' +
    '</span>';
