# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-13 17:37-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: extension.js:77 prefs.js:60
msgid "IP Address"
msgstr ""

#: extension.js:77
msgid "Loading IP Details"
msgstr ""

#: extension.js:78
msgid "Hostname"
msgstr ""

#: extension.js:79
msgid "City"
msgstr ""

#: extension.js:80
msgid "Region"
msgstr ""

#: extension.js:81
msgid "Country"
msgstr ""

#: extension.js:82
msgid "Location"
msgstr ""

#: extension.js:83
msgid "Org"
msgstr ""

#: extension.js:84
msgid "Postal"
msgstr ""

#: extension.js:85
msgid "Timezone"
msgstr ""

#: extension.js:104
msgid "VPN"
msgstr ""

#: extension.js:130
msgid "Off"
msgstr ""

#: extension.js:226
msgid "IP Details"
msgstr ""

#: extension.js:304
msgid "Settings"
msgstr ""

#: extension.js:314
msgid "Copy IP"
msgstr ""

#: extension.js:323
msgid "Refresh"
msgstr ""

#: extension.js:613
msgid "Error!"
msgstr ""

#: extension.js:613 extension.js:622
msgid "No Connection"
msgstr ""

#: extension.js:640
msgid "On"
msgstr ""

#: extension.js:696
msgid "Loading new map tile..."
msgstr ""

#: extension.js:704
msgid "Error getting map tile: "
msgstr ""

#: prefs.js:37
msgid "IP Finder"
msgstr ""

#: prefs.js:38
msgid "Displays useful information about your public IP Address"
msgstr ""

#: prefs.js:45 prefs.js:53
msgid "General"
msgstr ""

#: prefs.js:58
msgid "IP Address and Flag"
msgstr ""

#: prefs.js:59
msgid "Flag"
msgstr ""

#: prefs.js:67
msgid "Elements to show on the Panel"
msgstr ""

#: prefs.js:77
msgid "Left"
msgstr ""

#: prefs.js:78
msgid "Center"
msgstr ""

#: prefs.js:79
msgid "Right"
msgstr ""

#: prefs.js:81
msgid "Position in Panel"
msgstr ""

#: prefs.js:91
msgid "VPN Status"
msgstr ""

#: prefs.js:96
msgid "Show VPN Status"
msgstr ""

#: prefs.js:97
msgid ""
"Attempts to display VPN status. Works best when connecting VPN through GNOME"
msgstr ""

#: prefs.js:108
msgid "Icon on Panel + Text in Menu"
msgstr ""

#: prefs.js:109
msgid "Icon on Panel"
msgstr ""

#: prefs.js:110
msgid "Text in Menu"
msgstr ""

#: prefs.js:117
msgid "VPN status display options"
msgstr ""

#: prefs.js:134
msgid "Only show VPN status when VPN detected"
msgstr ""

#: prefs.js:148
msgid "Colorize VPN Icon based on VPN status"
msgstr ""

#: prefs.js:162
msgid "Colorize IP Address based on VPN status"
msgstr ""

#: prefs.js:169
msgid "Whitelisted Connections"
msgstr ""

#: prefs.js:170
msgid "Add a connection if its not automatically recognised by IP Finder"
msgstr ""

#: prefs.js:187
msgid "Add"
msgstr ""

#: prefs.js:203
msgid "Choose a connection to add to VPN Whitelist"
msgstr ""

#: prefs.js:210
msgid "Whitelisted VPN Connections"
msgstr ""

#: prefs.js:225
msgid "Delete"
msgstr ""

#: prefs.js:250
msgid "About"
msgstr ""

#: prefs.js:294
msgid "Version"
msgstr ""

#: prefs.js:303
msgid "Git Commit"
msgstr ""

#: prefs.js:312
msgid "GNOME Version"
msgstr ""

#: prefs.js:320
msgid "OS"
msgstr ""

#: prefs.js:342
msgid "Session Type"
msgstr ""

#: prefs.js:353
msgid "Credits"
msgstr ""
