/*
 * IP-Finder GNOME Extension by ArcMenu Team
 * https://gitlab.com/arcmenu-team/IP-Finder
 *
 * ArcMenu Team
 * Andrew Zaech https://gitlab.com/AndrewZaech
 * LinxGem33 (Andy C) https://gitlab.com/LinxGem33
 *
 * Find more from ArcMenu Team at
 * https://gitlab.com/arcmenu-team
 * https://github.com/ArcMenu
 *
 *
 * This file is part of IP Finder gnome extension.
 * IP Finder gnome extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * IP Finder gnome extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IP Finder gnome extension.  If not, see <http://www.gnu.org/licenses/>.
 */

const Me = imports.misc.extensionUtils.getCurrentExtension();
const {GLib, Gio, Soup} = imports.gi;

const TILE_ZOOM = 9;

Gio._promisify(Soup.Session.prototype, 'send_and_read_async');
Gio._promisify(Gio.File.prototype, 'replace_contents_bytes_async', 'replace_contents_finish');

const params = {
    id: `ip-finder/'${Me.metadata.version}`,
};

async function getIP(session, callback) {
    let message = Soup.Message.new_from_encoded_form(
        'GET',
        'https://ipinfo.io/ip',
        Soup.form_encode_hash(params)
    );

    let info;
    try {
        const bytes = await session.send_and_read_async(message, GLib.PRIORITY_DEFAULT, null);

        const decoder = new TextDecoder('utf-8');
        info = decoder.decode(bytes.get_data());
        if(message.statusCode === Soup.Status.OK)
            return [info, null];
        else{
            log(`IP-Finder getIp() failed with status code - ${message.statusCode}`)
            return [null, info];
        }

    } catch (e) {
        log(`IP-Finder getIp() error - ${e}`);
        return [null, e];
    }
}

async function getIPDetails(session, ipAddr) {
    let message = Soup.Message.new_from_encoded_form(
        'GET',
        `https://ipinfo.io/${ipAddr}/json`,
        Soup.form_encode_hash(params)
    );

    let info;
    try {
        const bytes = await session.send_and_read_async(message, GLib.PRIORITY_DEFAULT, null);

        const decoder = new TextDecoder('utf-8');
        info = JSON.parse(decoder.decode(bytes.get_data()));
        if(message.statusCode === Soup.Status.OK)
            return [info, null];
        else{
            log(`IP-Finder getIpDetails() failed with status code - ${message.statusCode}`)
            return [null, `${message.statusCode} - ${info['error'].title}. ${info['error'].message}.`];
        }
    } catch (e) {
        log(`IP-Finder getIpDetails() error - ${e}`)
        return [null, `IP-Finder getIpDetails() error - ${e}`];
    }
}

function getMapTileInfo(coordinates) {
    const [lat, lon] = coordinates.split(',').map(Number);
    const xTile = Math.floor((lon + 180.0) / 360.0 * (1 << TILE_ZOOM));
    const yTile = Math.floor((1.0 - Math.log(Math.tan(lat * Math.PI / 180.0) + 1.0 / Math.cos(lat * Math.PI / 180.0)) / Math.PI) / 2.0 * (1 << TILE_ZOOM));

    return({zoom: TILE_ZOOM, xTile, yTile});
}

async function getMapTile(session, tileInfo) {
    let file = Gio.file_new_for_path(Me.path + '/icons/latest_map.png');

    let message = Soup.Message.new_from_encoded_form(
        'GET',
        `https://a.tile.openstreetmap.org/${tileInfo}.png`,
        Soup.form_encode_hash(params)
    );

    let info;
    try {
        const bytes = await session.send_and_read_async(message, GLib.PRIORITY_DEFAULT, null);

        if(message.statusCode === Soup.Status.OK){
            info = bytes.get_data();
            const [success, _etag] = await file.replace_contents_bytes_async(info, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);
            return success ? [file, null] : [null, 'Error replacing map tile file.'];
        }
        else{
            log(`IP-Finder getMapTile() failed with status code - ${message.statusCode}`)
            return [null, message.statusCode];
        }
    } catch (e) {
        log(`IP-Finder getMapTile() error - ${e}`)
        return [null, message.statusCode];
    }
}
